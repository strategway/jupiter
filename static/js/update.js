/*
 * Copyright (C) 2015-2016 Strategway, http://strategway.com
 * 
 * This file is part of "Jupiter", the web application to display public transport
 * passenger data gathered with "Passim" mobile app.
 * 
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 * 
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Affero General Public License for more details.
 * 
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/

month_list = {
  '01': 'января', '02': 'февраля', '03': 'марта',
  '04': 'апреля', '05': 'мая', '06': 'июня',
  '07': 'июля', '08': 'августа', '09': 'сентября',
  '10': 'октября', '11': 'ноября', '12': 'декабря'
}
$( "#user_list" ).change(update_data);

function update_data() {
  var user_name = '';
  var regex = /\d{4}-\d{2}-\d{2}_\d{2}-\d{2}-\d{2}/;
  $( "select option:selected" ).each(function() {
    user_name = $( this ).text()
  });
  $.ajax({
    type: 'POST',
    url: '/view',
    dataType: 'json',
    contentType: 'application/json;charset=UTF-8',
    data: JSON.stringify({'select_user': user_name}),
    success: function(data) {
      $( "#user_data" ).empty();
      urls = data['data'];
      if (urls == null || urls.length == 0) {
        $( "#user_data" ).append(no_data());
      } else {
        last_time = '--time--';
        for (index = 0; index < urls.length; index++) {
          url_slice = urls[index].slice(0, -4);
          file_format = urls[index].slice(-4);
          fdate = Date.parseString(urls[index].match(regex), 'yyyy-MM-dd_HH-mm-ss');
          next_time = fdate.format('dd') + ' ' + month_list[fdate.format('MM')] + ' ' + fdate.format('yyyy');
          if (next_time !== last_time) {
            if (last_time != '--time--') {
              $( '#user_data' ).append('<br>');
            }
            $( "#user_data" ).append('<b>' + next_time +'</b><br>');
            last_time = next_time;
          }
          if (file_format == '.xml') {
            $( "#user_data" ).append(
              url_slice + ' ' +
              '<a href=/download/' + user_name + '/' + url_slice + '.csv' +
              '>Save as Microsoft Excel</a><br>'
            );
          } else {
            $( "#user_data" ).append(
              url_slice + ' ' +
              '<a href=/map/' + user_name + '/' + urls[index] + '/traffic>' + 'Passenger Traffic' + '</a> ' +
              '<a href=/map/' + user_name + '/' + urls[index] + '/transport>' + 'Transport Information' +
              '</a><br>'
            );
          }
        }
      }
    }
  });
}

function no_data() {
    return '<b>Данный пользователь не имеет загруженных данных</b>';
}
