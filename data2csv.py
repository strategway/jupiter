# Copyright (C) 2015-2016 Strategway, http://strategway.com
# 
# This file is part of "Jupiter", the web application to display public transport
# passenger data gathered with "Passim" mobile app.
# 
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU Affero General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
# 
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU Affero General Public License for more details.
# 
#   You should have received a copy of the GNU Affero General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
from __init__ import app
import xml.etree.ElementTree as ET
from datetime import datetime
from time import time as clock_time
import csv
import io
import os

month = {
    'January': 'января',
    'February': 'февраля',
    'March': 'марта',
    'April': 'апреля',
    'May': 'мая',
    'June': 'июня',
    'July': 'июля',
    'August': 'августа',
    'September': 'сентября',
    'October': 'октября',
    'November': 'ноября',
    'December': 'декабря'
}


def timestamp(data, format):
    return datetime.fromtimestamp(int(data)).strftime(format)


def stimestamp(data, format, replace_dict):
    lst = []
    for fmt_p in format:
        param = timestamp(data, fmt_p)
        if param in replace_dict:
            lst.append(replace_dict[param])
        else:
            lst.append(param)
    return ' '.join(lst)


def convert2csv(user, filename, encoding='utf-16'):
    csv_data = io.StringIO()
    writer = csv.writer(csv_data, delimiter='\t')
    path = os.path.join(app.config['UPLOAD_FOLDER'], user, filename)
    try:
        root = ET.parse(path).getroot()
    except:
        return None
    data = []
    time_stamp = root.find('save').get('timestamp')
    str_timestamp = stimestamp(time_stamp, ['%d', '%B', '%Y'], month)
    data.append(['Дата обследования:', str_timestamp + ' г.', '', '', '', ''])
    data.append(['Учётчик:', root.find('user').text, '', '', '', ''])
    data.append(['Остановка:', root.find('stop').text, '', '', '', ''])
    data.append(['Направление:', root.find('direction').text, '', '', '', ''])
    data.append(['Город:', root.find('city').text, '', '', '', ''])
    data.append(['', '', '', '', '', '', ''])
    data.append(['Время', '№ маршрута', 'Госномер', 'Вышло', 'Зашло', 'Проехал мимо'])
    last_time = None
    inp_count = 0
    out_count = 0
    for item in root.findall('vehiclelist/vehicle'):
        prev_time = timestamp(item.get('timestamp'), '%H:00')
        time = timestamp(item.get('timestamp'), '%H:%M')
        route = item.get('name')
        licence = '' if item.get('license_plate') is None else item.get('license_plate')
        out = '' if item.get('out') is None else item.get('out')
        inp = '' if item.get('in') is None else item.get('in')
        passed = '' if item.get('passed_by') is None else 'Да'
        inp_count += 0 if inp == '' else int(inp)
        out_count += 0 if out == '' else int(out)
        if last_time != prev_time:
            last_time = prev_time
            data.append(['_---{}---'.format(prev_time), '', '', '', '', ''])
        data.append([time, route, licence, out, inp, passed])
    data.append(['', '', '', '', '', '', ''])
    data.append(['Смена завершена:', timestamp(root.find('save').get('timestamp'), '%H:%M'), '', '', '', ''])
    data.append(['', '', '', '', '', '', ''])
    data.append(['Пассажирообмен остановки за смену:', '', '', '', '', '', ''])
    data.append(['Зашло:', inp_count, '', '', '', '', ''])
    data.append(['Вышло:', out_count, '', '', '', '', ''])
    data.append(['Всего:', inp_count + out_count, '', '', '', '', ''])
    data.append(['', '', '', '', '', '', ''])
    data.append(['Отчёт сформирован:', stimestamp(clock_time(),
        ['%d', '%B', '%Y', 'г. %H:%M'], month), '', '', '', '', ''])
    for row in data:
        writer.writerow(row)
    return csv_data.getvalue().encode(encoding)
