# Copyright (C) 2015-2016 Strategway, http://strategway.com
# 
# This file is part of "Jupiter", the web application to display public transport
# passenger data gathered with "Passim" mobile app.
# 
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU Affero General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
# 
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU Affero General Public License for more details.
# 
#   You should have received a copy of the GNU Affero General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
from __init__ import app
from flask import render_template, request, Response, jsonify
from werkzeug import secure_filename
from auth import requires_auth
from model import User, UserRole
from re import search
import data2geojson as d2g
import data2csv as d2c
import logging
import os


def sortByDate(lst):
    lst.sort(key=lambda x: search(r'\d{4}-\d{2}-\d{2}_\d{2}-\d{2}-\d{2}', x).group(0))
    return lst


@app.route('/upload', methods=['POST', 'GET'])
@requires_auth
def upload():
    logging.warning(request.method)
    if request.method == 'POST':
        upload_file = request.files['file']
        username = request.authorization.username
        if upload_file and bool(upload_file.filename):
            folder = os.path.join(app.config['UPLOAD_FOLDER'], username)
            filename = os.path.splitext(
                os.path.join(folder, secure_filename(upload_file.filename))
            )[0]
            # use read becasue upload_file is FileStorage
            mimetype = d2g.magic.from_buffer(upload_file.read())
            # magic not seek to file 0 position
            upload_file.seek(0, 0)
            if mimetype_format == d2g.xml_format:
                filename += '.xml'
            elif mimetype_format == d2g.csv_format:
                filename += '.csv'
            else:
                logging.warning('bad mimetype: {}'.format(upload_file.mimetype))
                return render_template('400.html'), 400
            if not os.path.exists(folder):
                os.makedirs(folder)
            if not os.path.isfile(filename):
                upload_file.save(filename)
            else:
                data = 'File {} exists!'.format(upload_file.filename)
                return Response(response=data, status=403, mimetype='text/plain')
            data = 'File {} succesfully uploaded!'.format(upload_file.filename)
            return Response(response=data, status=200, mimetype='text/plain')
        else:
            logging.warning('upload file not exits')
            return render_template('400.html'), 400
    elif request.method == 'GET':
        return render_template('upload.html')
    else:
        logging.warning('unsupported request method: {}'.format(request.method))
        return render_template('403.html'), 403


@app.route('/map/<username>/<data>/<action>')
@requires_auth
def map(username, data, action):
    e_action = ''
    current_user = request.authorization.username
    user = User.query.filter_by(name=current_user).first()
    if UserRole(user.role) == UserRole.FOREMAN or current_user == username:
        if action == 'traffic':
            e_action = d2g.MapAction.TRAFFIC
        elif action == 'transport':
            e_action = d2g.MapAction.TRANSPORT
        else:
            return render_template('400.html'), 400
        load_file = os.path.join(app.config['UPLOAD_FOLDER'], username, secure_filename(data))
        if not os.path.isfile(load_file):
            return render_template('404.html'), 404
        (render_data, service) = d2g.convert_geojson(load_file, e_action)
        if render_data is None:
            return 'Incorrect encoding of the input file', 400
        return render_template('map.html', geojson=render_data, service=service)
    else:
        return render_template('403.html'), 403


@app.route('/view', methods=['GET', 'POST'])
@requires_auth
def user_view():
    if request.method == 'POST':
        json_data = request.get_json()
        if json_data.get('select_user'):
            current_user = json_data['select_user']
            user = User.query.filter_by(name=request.authorization.username).first()
            if UserRole(user.role) == UserRole.FOREMAN:
                path = os.path.join(app.config['UPLOAD_FOLDER'], current_user)
                data = sortByDate(os.listdir(path)) if os.path.exists(path) else []
                return jsonify(data=data)
            else:
                return render_template('403.html'), 403
        else:
            return render_template('403.html'), 403
    elif request.method == 'GET':
        args = {}
        current_user = request.authorization.username
        user = User.query.filter_by(name=current_user).first()
        if UserRole(user.role) == UserRole.FOREMAN:
            args['foreman'] = True
            args['users'] = []
            for user in User.query.all():
                args['users'].append(user.name)
        args['user'] = current_user
        return render_template('view.html', args=args)


@app.route('/download/<username>/<filename>')
@requires_auth
def download(username, filename):
    current_user = request.authorization.username
    user = User.query.filter_by(name=current_user).first()
    if UserRole(user.role) == UserRole.FOREMAN or current_user == username:
        input_file = os.path.splitext(filename)[0] + '.xml'
        response_data = d2c.convert2csv(username, input_file)
        if response_data is None:
            return render_template('400.html'), 400
        response = Response(response_data, mimetype='text/csv')
        response.headers['Content-Disposition'] = "inline; filename=" + filename
        return response
    else:
        return render_template('403.html'), 403


@app.route('/')
def index():
    return render_template('index.html', data=app.config['APP_INFO'])


@app.before_first_request
def setup_logging():
    if not app.debug:
        app.logger.addHandler(logging.StreamHandler())
        app.logger.setLevel(logging.WARN)

if __name__ == '__main__':
    app.run()
