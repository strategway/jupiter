# Copyright (C) 2015-2016 Strategway, http://strategway.com
# 
# This file is part of "Jupiter", the web application to display public transport
# passenger data gathered with "Passim" mobile app.
# 
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU Affero General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
# 
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU Affero General Public License for more details.
# 
#   You should have received a copy of the GNU Affero General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
from geographiclib.geodesic import Geodesic
from datetime import datetime
from csv import DictReader
from enum import Enum
from math import log10, sqrt
from colorsys import hls_to_rgb
from magic import Magic
import geojson as gs

MIN_SPEED = 15
MAX_SPEED = 40

xml_format = b'application/xml'
csv_format = b'text/plain'

magic = Magic(mime=True)

# function: calculate distance from a to b
# input:
#   a -- first point
#   b -- second point
# output:
#   distance in sphere
def getDistance(a, b):
    return Geodesic.WGS84.Inverse(a[1], a[0], b[1], b[0])['s12']

class MapAction(Enum):
    TRAFFIC = 0
    TRANSPORT = 1


def remove_bom(item):
    return item.replace(u'\ufeff', '')


def get_time(timestamp):
    return datetime.strptime(timestamp, '%Y-%m-%d %H:%M:%S')


def get_color(speed):
    if speed >= MAX_SPEED:
        depth = 0.47
    elif speed <= MIN_SPEED:
        depth = 0.0
    else:
        depth = 0.47 * (speed - MIN_SPEED) / (MAX_SPEED - MIN_SPEED)
    (R, G, _) = hls_to_rgb(depth, 0.5, 1.0)
    R, G = int(R * 255), int(G * 255)
    return '#{:02x}{:02x}{:02x}'.format(R, G, 0)


def dict_cleaner(enc, row):
    if enc == 'utf8':
        n_dict = row
    else:
        n_dict = {}
        for key, item in row.items():
            key = remove_bom(key)
            item = remove_bom(item)
            n_dict[key] = item
    return n_dict


def check_enc(filename):
    try:
        filedata = open(filename, encoding='utf16').read()
        return 'utf16'
    except:
        filedata = open(filename, encoding='utf8').read()
        return 'utf8'
    return None


def csv_loader(filename, action):
    feature_pass, feature_data = [], []
    encoding = check_enc(filename)
    if encoding is None:
        return None
    f = open(filename, encoding=encoding)
    inp_raw = DictReader(f, delimiter='\t')
    passengers = 0
    t1, t2, p1, p2 = None, None, None, None
    service = {
        'total_passenger': 0,
        'max_passenger': 0,
        'max_speed': MAX_SPEED,
        'avg_speed': (MAX_SPEED + MIN_SPEED) // 2,
        'min_speed': MIN_SPEED
    }
    for row in inp_raw:
        n_dict = dict_cleaner(encoding, row)
        passenger_in = n_dict['Passengers in']
        passenger_out = n_dict['Passengers out']
        last_passengers = passengers
        passengers += int(passenger_in) - int(passenger_out)
        service['total_passenger'] += int(passenger_in)
        service['max_passenger'] = max(service['max_passenger'], passengers)
        service['route_number'] = n_dict['Route\'s #']
        time_stamp = n_dict['Date'] + ' ' + n_dict['Time']
        coords = [float(n_dict['Longitude']), float(n_dict['Latitude'])]
        point = gs.Feature(geometry=gs.Point(coords),
            properties={
                'timestamp': time_stamp, 'in': passenger_in, 'out': passenger_out,
                'now': passengers
            })
        feature_pass.append(point)
        if t1 is None:
            t1 = get_time(time_stamp)
            p1 = coords
        else:
            t2 = get_time(time_stamp)
            p2 = coords
            if action is MapAction.TRAFFIC:
                service['action'] = 'traffic'
                line = gs.Feature(geometry=gs.LineString([p1, p2]),
                    properties={
                        'color': '#00ff00',
                        'opacity': 0.6,
                        'weight': 2 + last_passengers // 2,
                        'count': last_passengers
                    })
            elif action is MapAction.TRANSPORT:
                service['action'] = 'transport'
                distance = getDistance(p1, p2)
                delta = (t2 - t1).seconds
                speed = (distance / delta) * 3.6
                line = gs.Feature(geometry=gs.LineString([p1, p2]),
                    properties={
                        'color': get_color(speed),
                        'speed': speed,
                        'time': delta / 60,
                        'distance': distance
                    })
            feature_data.append(line)
            t1, p1 = t2, p2
    if feature_data != []:
        return (gs.FeatureCollection([
            gs.FeatureCollection(feature_data),
            gs.FeatureCollection(feature_pass)
        ]), service)
    else:
        return (gs.FeatureCollection(feature_pass), service)


def xml_loader(filename, action):
    return (None, None)


def convert_geojson(filename, action):
    mimetype = magic.from_file(filename)
    if mimetype == xml_format:
        return xml_loader(filename, action)
    elif mimetype == csv_format:
        return csv_loader(filename, action)
    else:
        return (None, None)
