# Copyright (C) 2015-2016 Strategway, http://strategway.com
# 
# This file is part of "Jupiter", the web application to display public transport
# passenger data gathered with "Passim" mobile app.
# 
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU Affero General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
# 
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU Affero General Public License for more details.
# 
#   You should have received a copy of the GNU Affero General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
from __init__ import db
from enum import Enum


class UserRole(Enum):
    USER = 0
    FOREMAN = 1


class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64))
    password = db.Column(db.String(8))
    role = db.Column(db.Integer)

    def __init__(self, name, password, role=UserRole.USER):
        self.name = name
        self.password = password
        self.role = role.value

    def __repr__(self):
        return "<User('{}', '{}', {})>".format(self.name, self.password, self.role)
