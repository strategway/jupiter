# Copyright (C) 2015-2016 Strategway, http://strategway.com
# 
# This file is part of "Jupiter", the web application to display public transport
# passenger data gathered with "Passim" mobile app.
# 
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU Affero General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
# 
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU Affero General Public License for more details.
# 
#   You should have received a copy of the GNU Affero General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
import os

STATIC_FOLDER = 'static'
UPLOAD_FOLDER = os.path.join(os.path.dirname(__file__), 'data')
SQLALCHEMY_DATABASE_URI = 'sqlite:////' + os.path.dirname(__file__) + '/database.db'
SECRET_KEY = 'jupiter-project-is-awesome'
APP_INFO = {'app_name': 'Passim', 'version': '0.x.x'}
