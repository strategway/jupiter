# Copyright (C) 2015-2016 Strategway, http://strategway.com
# 
# This file is part of "Jupiter", the web application to display public transport
# passenger data gathered with "Passim" mobile app.
# 
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU Affero General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
# 
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU Affero General Public License for more details.
# 
#   You should have received a copy of the GNU Affero General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
from __init__ import db
from model import User
import random

ALPHABET = 'abcdefghijkmnpqrstuvwxyz123456789'
GEN_COUNT = 500
GEN_SIZE = 8


def generator(alphabet, size):
    return ''.join([random.choice(alphabet) for n in range(size)])

if __name__ == '__main__':
    for index in range(GEN_COUNT):
        username = generator(ALPHABET, GEN_SIZE)
        password = generator(ALPHABET, GEN_SIZE)
        print('{:4} {} {}'.format(index+1, username, password))
        db.session.add(User(username, password))
    db.session.commit()
