#!/bin/bash

# Copyright (C) 2015-2016 Strategway, http://strategway.com
# 
# This file is part of "Jupiter", the web application to display public transport
# passenger data gathered with "Passim" mobile app.
# 
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU Affero General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
# 
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU Affero General Public License for more details.
# 
#   You should have received a copy of the GNU Affero General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.

PROJECT_NAME="<CHANGE-ME>"
FLASKDIR="/var/www/dev/$PROJECT_NAME"
VENVDIR=/var/www/dev/env
SOCKFILE="$FLASKDIR/$PROJECT_NAME.sock"
USER=www-data
GROUP=www-data
NUM_WORKERS=3

if [[ "$NAME" = "<CHANGE-ME>" ]]; then
    echo "[!] Please change project name parameter"
    exit
fi

echo "Starting $PROJECT_NAME"

# activate the virtualenv
cd $VENVDIR
source bin/activate

export PYTHONPATH=$FLASKDIR:$PYTHONPATH

# Create the run directory if it doesn't exist
RUNDIR=$(dirname $SOCKFILE)
test -d $RUNDIR || mkdir -p $RUNDIR

# Start your unicorn
exec gunicorn main:app \
    --workers $NUM_WORKERS \
    --user=$USER \
    --group=$GROUP \
    --log-level=warning \
    -m 007 \
    --bind=unix:$SOCKFILE
