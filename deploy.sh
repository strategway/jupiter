#!/bin/bash

# Copyright (C) 2015-2016 Strategway, http://strategway.com
# 
# This file is part of "Jupiter", the web application to display public transport
# passenger data gathered with "Passim" mobile app.
# 
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU Affero General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
# 
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU Affero General Public License for more details.
# 
#   You should have received a copy of the GNU Affero General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.

DATE="`date +"%F_%H-%M-%S"`"
PROJECT_NAME="<CHANGE-ME>"
JUPITER_URL=http://oriole.strategway.com/strategway/jupiter.git
JUPITER_CWD="`dirname "$(readlink -f "$0")"`"
JUPITER_DIR=$JUPITER_CWD
JUPITER_OLD_DIR="$JUPITER_CWD-$DATE"
USER=www-data
GROUP=www-data

if [ "$PROJECT_NAME" = "<CHANGE-ME>" ]; then
    echo "[!] Please change PROJECT_NAME variable"
    exit
fi

sudo echo "Starting deploying..."

if [[ $? -ne 0 ]]; then
    echo "[error] sudo check failure"
    exit
fi

# stop project service
echo "[sudo] Stopping $PROJECT_NAME service"
sudo supervisorctl stop $PROJECT_NAME

if [[ $? -ne 0 ]]; then
    echo "[error] can't stop project service"
    exit
fi

# move cwd to new dir
echo "[sudo] Moving current project"
cd ..
sudo mv $JUPITER_CWD $JUPITER_OLD_DIR

if [[ $? -ne 0 ]]; then
    echo "[error] can't rename folder"
    exit
fi

# packing old project
echo "[user] Packing old project"
tar czf ./"$PROJECT_NAME-$DATE.tar.gz" $JUPITER_OLD_DIR

if [[ $? -ne 0 ]]; then
    echo "[error] can't archive project"
    exit
fi

# cloning repo
echo "[user] Cloning repo"
sudo -u www-data bash -c "umask 002; git clone $JUPITER_URL $JUPITER_DIR"
if [[ $? -ne 0 ]]; then
    echo "[error] Can't clone project"
    exit
fi

# change current working dir
echo "[user] Change current working directory"
cd $JUPITER_DIR

if [[ $? -ne 0 ]]; then
    echo "[error] can't change working dir"
    exit
fi

sudo chmod -R g=rwx .git

if [[ $? -ne 0 ]]; then
    echo "[error] can't change .git group permission"
    exit
fi

# removing unused data
echo "[user] Removing unused files"
rm -rf .git data database.db

if [[ $? -ne 0 ]]; then
    echo "[error] can't remove unused files"
    exit
fi

# create directories
echo "[user] Creating directories"
sudo -u www-data bash -c "umask 002; mkdir logs"

if [[ $? -ne 0 ]]; then
    echo "[error] can't create log directory"
    exit
fi

# change current working dir
echo "[user] Change current working directory"
cd ..

if [[ $? -ne 0 ]]; then
    echo "[error] can't change current directory"
    exit
fi

# moving new project
echo "[sudo] Move new project to production"
sudo cp $JUPITER_OLD_DIR/database.db $JUPITER_DIR/

if [[ $? -ne 0 ]]; then
    echo "[error] can't copy database.db file"
    exit
fi

sudo cp -r $JUPITER_OLD_DIR/data $JUPITER_DIR/

if [[ $? -ne 0 ]]; then
    echo "[error] can't copy project data folder"
    exit
fi

sudo rm -r $JUPITER_OLD_DIR/

if [[ $? -ne 0 ]]; then
    echo "[error] can't remove old project folder"
    exit
fi

sudo chown -R $USER:$GROUP $JUPITER_DIR/

if [[ $? -ne 0 ]]; then
    echo "[error] can't change owner"
    exit
fi

# start project service
echo "[sudo] Starting $PROJECT_NAME service"
sudo supervisorctl start $PROJECT_NAME

if [[ $? -ne 0 ]]; then
    echo "[error] can't start project service"
    exit
fi
